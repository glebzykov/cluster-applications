#!/bin/bash -e

set -x

export TILLER_NAMESPACE=${TILLER_NAMESPACE:-gitlab-managed-apps}

if [ -z "$1" ]; then
  echo "Missing helmfile argument"
  exit 1
fi

helmfile=$1

export HELM_HOST="localhost:44134"

echo "Ensuring $TILLER_NAMESPACE namespace exists..."

if ! kubectl get namespace "$TILLER_NAMESPACE" > /dev/null 2>&1; then
  kubectl create namespace "$TILLER_NAMESPACE"
  kubectl label namespace "$TILLER_NAMESPACE" app.gitlab.com/managed_by=gitlab
fi

echo "Checking Tiller..."

nohup tiller -listen ${HELM_HOST} > tiller.log 2>&1 &
echo "Tiller is listening on ${HELM_HOST}"

if ! helm version --debug; then
  echo "Failed to init Tiller."
  return 1
fi

export GITLAB_MANAGED_APPS_FILE=${GITLAB_MANAGED_APPS_FILE:-$CI_PROJECT_DIR/.gitlab/managed-apps/config.yaml}

helmfile_optional_args=()
if [[ -n "$SKIP_DEPS" ]]; then
  helmfile_optional_args+=('--skip-deps')
fi

helmfile --file "$helmfile" apply --suppress-secrets "${helmfile_optional_args[@]}"

# List helm releases for cluster_applications artifact
helm ls --output json > gl-cluster-applications.json
